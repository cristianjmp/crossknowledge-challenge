<?php
require 'service/RedisService.php';
use Service\RedisService;

class SimpleJsonRequest
{
    private static function makeRequest(string $method, string $url, array $parameters = null, array $data = null)
    {
        $opts = [
            'http' => [
                'method'  => $method,
                'header'  => 'Content-type: application/json',
                'content' => $data ? json_encode($data) : null,
                'x-api-key' => 'pKrFw9wjvw7qv36zuacDvaFqMhynqHM29rp9QeyG'
            ]
        ];

        $url .= ($parameters ? '?' . http_build_query($parameters) : '');
        return file_get_contents($url, false, stream_context_create($opts));
    }

    public static function get(string $url, array $parameters = null)
    {
        return self::cachedRequest('GET', $url, $parameters);
    }

    public static function post(string $url, array $parameters = null, array $data)
    {
        return self::cachedRequest('POST', $url, $parameters, $data);
    }

    public static function put(string $url, array $parameters = null, array $data)
    {
        return self::cachedRequest('PUT', $url, $parameters, $data);
    }   

    public static function patch(string $url, array $parameters = null, array $data)
    {
        return self::cachedRequest('PATCH', $url, $parameters, $data);
    }

    public static function delete(string $url, array $parameters = null, array $data = null)
    {
        return self::cachedRequest('DELETE', $url, $parameters, $data);
    }

    private static function cachedRequest(string $method, string $url, array $parameters = null, array $data = null) {
        $requestData = RedisService::GetData($method);
        if (empty($requestData)) {
            $requestData = json_decode(self::makeRequest($method, $url, $parameters, $data));
            RedisService::SetData($method, $requestData);
        }
        return $requestData;
    }
}