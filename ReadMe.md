# Crossknowledge Challenge  
## To execute this challenge, execute the following steps:  
>- Execute the following command at the project root directory, to build and up the Docker environment:  
    - make magic  
>- To execute the Redis Challenge, access:  
    - http://localhost:8080/index.php?name=Cristian&lang=en&city=BC&state=SC&outraCoisa=bolinhas  
    - Also, you can pass the params in a query string using Insomnia for example.  
>- To execute the Date Format Challenge, access:  
    - http://localhost:8080/date-format.html
>- To execute the Avengers Challenge, access:  
    - http://localhost:8080/component.html