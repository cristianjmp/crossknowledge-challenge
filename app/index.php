<?php
    require 'request.php';

    parse_str($_SERVER['QUERY_STRING'], $queryStringParams);
    $url = "https://0fkrfcl7eb.execute-api.us-east-2.amazonaws.com/teste/message/{$queryStringParams['name']}";

    $requestMethodMap = [
        'GET' => function ($url, $queryStringParams) {
            return SimpleJsonRequest::get($url, $queryStringParams);
        }
    ];

    if (!isset($requestMethodMap[$_SERVER["REQUEST_METHOD"]])) {
        http_response_code(404);
        exit(404);
    }

    echo "Current Timestamp: " . time() . "<br/>";
    echo "<pre>";
    print_r($requestMethodMap[$_SERVER["REQUEST_METHOD"]]($url, $queryStringParams));
    echo "</pre>";