build:
	docker-compose up --build --no-start

run:
	docker-compose run -d --rm --name execution-challenge -p 8080:80 challenge
	docker-compose run -d --rm --name execution-redis -e ALLOW_EMPTY_PASSWORD=yes -p 7001:6379 redis

clean:
	docker rm -f execution-redis
	docker rm -f execution-challenge

magic: build run