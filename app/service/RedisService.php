<?php

namespace Service;

use Couchbase\Exception;
use Redis;

class RedisService
{
    /* Life tome of the stored value in seconds*/
    private const LIFE_TIME_DATA = '10';

    private static function RedisConnection()
    {
        $redis = new Redis();
        $redis->connect('172.18.0.3', 6379);
        return $redis;
    }

    public static function GetData($searchKey)
    {
        try {
            $redisClient = RedisService::RedisConnection();
            return $redisClient->exists($searchKey) ? unserialize($redisClient->get($searchKey)) : '';
        } catch (Exception $e) {
            syslog(LOG_ALERT, "Redis error: {$e->getMessage()}");
            return '';
        }
    }

    public static function SetData($searchKey, $value)
    {
        try {
            $redisClient = RedisService::RedisConnection();
            return $redisClient->setex($searchKey, self::LIFE_TIME_DATA, serialize($value));
        } catch (Exception $e) {
            syslog(LOG_ALERT, "Redis error: {$e->getMessage()}");
            return false;
        }
    }

    public static function DeleteData($searchKey)
    {
        try {
            $redisClient = RedisService::RedisConnection();
            return $redisClient->del($searchKey);
        } catch (Exception $e) {
            syslog(LOG_ALERT, "Redis error: {$e->getMessage()}");
            return false;
        }
    }

}